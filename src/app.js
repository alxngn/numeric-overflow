'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => {
    // size: TODO: somehow specify n bytes of req.query.amount?
    // bitshift?

    if (approval(parseInt(req.query.amount))) {
        es.status(400).end(req.query.amount + ' requires approval.');
    } else {
        res.status(200).end(req.query.amount + ' does not require approval.');
    }
});

var isSafe32BitInt = value => {
    try {
        var valueNum = Number(value);
        if (isNaN(valueNum)) return false;
        return Number(value) < (Math.pow(2,32)/2) && Number(value) >= -(Math.pow(2,32)/2);
    } catch (ex) {
        return false;
    }
}

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {
    var threshold = 1000;
    var surcharge = 10;
    // type
    console.log('entered approval');

    if (!typeof(value) === 'number' || !Number.isSafeInteger(value)) {
        throw TypeError('approved value must be a number');
    }

    if (value < 0 || !isSafe32BitInt(value+surcharge)) {
        throw RangeError('input value outside accepted range');
    }

    console.log('after checks');

    var amount = Int32Array.from([value],x=>parseInt(x+surcharge));
    //var amount = parseInt(value) + surcharge;

    // console.log(amount[0]);
    if (amount[0] >= threshold) {
        return true;
    };
    return false;
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval };
